[![npm](https://img.shields.io/npm/v/@ioniczoo/bird-format-pipe.svg)](https://www.npmjs.com/package/@ioniczoo/bird-format-pipe)
[![npm](https://img.shields.io/npm/dt/@ioniczoo/bird-format-pipe.svg)](https://www.npmjs.com/package/@ioniczoo/bird-format-pipe)
[![npm](https://img.shields.io/npm/l/@ioniczoo/bird-format-pipe.svg?style=flat-square)](https://www.npmjs.com/package/@ioniczoo/bird-format-pipe)

<img src="https://gitlab.com/IonicZoo/bird-format-pipe/raw/master/img.jpg" width="20%" height="auto" alt="bird" title="bird">

# Bird Format Pipe

Format Pipe for Ionic: Birds forms magnific draws on sky, each species with its characteristic pattern. Our bird provides a simple way to display patterns (masks) using Pipe.


## Install

```bash
npm install @ioniczoo/bird-format-pipe --save
```

## Import

```ts
import { NgModule } from '@angular/core';
import { FormatPipeModule } from '@ioniczoo/bird-format-pipe';

@NgModule({
  imports: [
    FormatPipeModule
  ],
  declarations: []
})
export class AppModule {}
```

## Examples

```html
<!-- CPF Format -->
<span>{{ cpf | format:'999.999.999-99' }}</span><br/>

<!-- Phone Variable Format -->
<span>{{ tel | format:'(99) 9 9999-9999' }}</span><br/>

<!-- Phone String Format -->
<span>{{ '719988776655' | format:'(99) 9 9999-9999' }}</span><br/>

<!-- Phone Number Format -->
<span>{{ 719988776655 | format:'(99) 9 9999-9999' }}</span><br/>
```

## Author

[André Argôlo](https://argolo.gitlab.io)

## Contribute

[Create issues and request pull-request.](https://gitlab.com/IonicZoo/bird-format-pipe/blob/master/CONTRIBUTING.md)

## License

[GNU General Public License v3.0](https://gitlab.com/IonicZoo/bird-format-pipe/blob/master/LICENSE)
